Some Sample Camel Code
======================

This project demonstrates a few ideas, stemming from a code review of a Spring Boot - Camel project.

In particular:


Decomposed Camel Context
------------------------
Rather than a single camel-context.xml that is imported in the main `Application` class, this project relies on the integration between Camel and Spring Boot to auto configure routes found under the `camel/` folder on the classpath. See [http://camel.apache.org/spring-boot.html#SpringBoot-AddingXMLRoutes] 

This project has two such files `main-route.xml` contains the core route, and `logging-route.xml` contains a supporting route for business logging. `logging-route.xml` could have been included from a project dependency, allowing for modular inclusion of capabilities.
 

Simplified Rest Configuration
-----------------------------
This project eliminates the `<restConfiguration>` elements from the camel context XML files, and moves them to an `@Configuration` bean named `RestConfiguration`. This streamlines the config and allows for the direct use of `application.properties` configured elements without needing a `BridgePropertyPlaceholderConfigurer`

Additionally, the REST DSL entries have been decomposed into a `camel-rest/` folder. See [http://camel.apache.org/spring-boot.html#SpringBoot-AddingRest-DSL]


Bean Binding for Setting Headers
--------------------------------
When there is a need to perform repeated setting of headers in a route, as is needed in this example for prepping the business logging data for the logging-route, it can be helpful to extract that logic into a bean for reuse. `LoggingUtils` demonstrates the implementation of such a bean, and includes a sample of how it may be invoked from the XML.


Setting Cache Values to Headers Via Enrich EIP
----------------------------------------------
Rather than doing a lot of shuffling to load values from the cache and then transfer them to a particular header, it is possible to enrich the original message directly via the [enrich](http://camel.apache.org/content-enricher.html#ContentEnricher-enrich-dsl) EIP.

In order to take advantage of this, we need to develop a custom aggregation strategy for the enricher to use. This can be found in [SetCacheValueToHeader.java](src/main/java/rh/review/cache/SetCacheValueToHeaderStrategy.java).

The `aggregate` method checks the exchange for a successfully retrieved cache value and sets it to a header of the same name as the CamelCacheKey used to fetch the value. This implementation also accepts a default value to be used in the event of a cache miss.

Using this approach, it is possible to load a cache value into an exchange header without any impact on the body of the original message. If additional processing of the cached value is needed before it is set, this can be accomplished by creating a route to fetch the value and process it, which will then be fed into the enricher.

Both of these use cases are demonstrated in [main-route.xml](src/main/resources/camel/main-route.xml)