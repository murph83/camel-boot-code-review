package rh.review.cache;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

/**
 * An example processor component used in the cache-retrieval route
 */
@Component
public class SplitBodyProcessor implements Processor{

    /**
     * Get the IN message body and split it by `~`. Return the second half.
     * @param exchange
     * @throws Exception
     */
    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        String[] split = body.split("~");
        String substring = split[1];
        exchange.getIn().setBody(substring);
    }
}
