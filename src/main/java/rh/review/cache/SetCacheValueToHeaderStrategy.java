package rh.review.cache;

import org.apache.camel.Exchange;
import org.apache.camel.component.cache.CacheConstants;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.stereotype.Component;

/**
 * Aggregation strategy that sets the value of a cache lookup to the header of the same name as the cache key on the current exchange. 
 * Accepts a default value in headers.CacheValueDefault
 *
 */
@Component("setCacheValueToHeader")
public class SetCacheValueToHeaderStrategy implements AggregationStrategy {

    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {

        boolean cacheElementFound = newExchange.getIn().getHeader(CacheConstants.CACHE_ELEMENT_WAS_FOUND, boolean.class);

        // If found, set cache element, otherwise, pull the default value from the old exchange
        String valueToSet = cacheElementFound
                ? newExchange.getIn().getBody(String.class)
                : oldExchange.getIn().getHeader("CacheValueDefault", String.class);

        String targetHeader = oldExchange.getIn().getHeader("CamelCacheKey", String.class);

        oldExchange.getIn().setHeader(targetHeader, valueToSet);

        return oldExchange;
    }
}
