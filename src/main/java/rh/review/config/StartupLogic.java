package rh.review.config;

import org.apache.camel.CamelContext;
import org.apache.camel.management.event.CamelContextStartedEvent;
import org.apache.camel.support.EventNotifierSupport;
import org.springframework.stereotype.Component;

import java.util.EventObject;

@Component
public class StartupLogic extends EventNotifierSupport{

    public void notify(EventObject event) throws Exception {
        CamelContext camelContext = ((CamelContextStartedEvent)event).getContext();
        // perform initialization logic here
        camelContext.startRoute("get-api-resource");
    }

    public boolean isEnabled(EventObject event) {
        return event instanceof CamelContextStartedEvent;
    }
}
