package rh.review.logging;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

/**
 * This class demonstrates the use of a Bean component in the camel route to streamline the XML
 */
@Component("logging")
public class LoggingUtils {

    /**
     * Invoke with the following camel pattern:
     * <to uri="bean:logging?method=setHeaders(*,'STATUS','STATUS DETAILS')"/>
     */
    public void setHeaders(Exchange ex, String status, String details) {
        ex.getIn().setHeader("ProcessStatus", status);
        ex.getIn().setHeader("ProcessStatusDetails", details);
        ex.getIn().setHeader("uuid", ex.getExchangeId());
    }
}
